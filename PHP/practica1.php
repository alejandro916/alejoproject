<DOCTYPE html>
<html lang="ca">
<head>
<meta charset="utf-8">
<title>Practica1</title>
</head>
<body>
<?php
 $varArray1 = array("a","be","ce","de");
 $varArray2 = array(
		"clau1"=>"e",
		"clau2"=>"efe",
		"clau3"=>array("Un","Dos","Tres"),
		"clau4"=>"hache"
		);
 echo "<p><pre>";
 echo "<h2>Array Normal</h2>";
 print_r($varArray1);
 echo "<h2>Array Associativa</h2>";
 print_r($varArray2);
 "</pre></p>";

 echo "<br>";
 $variable1 = 25;
 $variable2 = 15;

 echo "Valor màxim(" . $variable1 . ", " . $variable2 . "): " . max($variable1, $variable2);
 echo "<br>";
 echo "Arrel quadrada(" . $variable1 . "): " . sqrt($variable1);

?>
</body>
</html>

